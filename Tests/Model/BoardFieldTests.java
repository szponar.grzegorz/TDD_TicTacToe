package Model;

import org.junit.Test;

import static View.Board.printBoard;
import static org.junit.Assert.*;


public class BoardFieldTests {

    @Test
    public void boardFieldsIsMadeOfField(){
        Board emptyFieldBoard = new BoardField(3,3, FieldToken.EMPTY);
        Coordinate coordinate = new Coordinate(2,2);

        assertEquals("Empty board should consist Empty field", FieldToken.EMPTY, emptyFieldBoard.getValue(coordinate));

        BoardField crossFieldBoard = new BoardField(3,3, FieldToken.CROSS);
        assertEquals("Cross board should consist Cross field", FieldToken.CROSS, crossFieldBoard.getValue(coordinate));

        BoardField noughtFieldBoard = new BoardField(3,3, FieldToken.NOUGHT);
        assertEquals("Nought board should consist Nought field", FieldToken.NOUGHT, noughtFieldBoard.getValue(coordinate));
    }

    @Test
    public void areThreeTokensHorizontally(){
        BoardField board = new BoardField(3, 3, FieldToken.EMPTY);
        Field fieldCross = FieldToken.CROSS;

        board.setValue(new Coordinate(0,1),fieldCross);
        board.setValue(new Coordinate(1,1),fieldCross);
        board.setValue(new Coordinate(2,1),fieldCross);

        assertEquals(FieldToken.CROSS, board.areThreeSameFieldsInLine(new Coordinate(2, 1)));
    }

    @Test
    public void areThreeTokensVertically(){
        BoardField board = new BoardField(3, 3, FieldToken.EMPTY);
        Field fieldNought = FieldToken.NOUGHT;

        board.setValue(new Coordinate(0,0),fieldNought);
        board.setValue(new Coordinate(0,1),fieldNought);
        board.setValue(new Coordinate(0,2),fieldNought);

        assertEquals(FieldToken.NOUGHT, board.areThreeSameFieldsInLine(new Coordinate(0,2)));
    }

    @Test
    public void areThreeTokensInLeftDiagonal(){
        BoardField board = new BoardField(3, 3, FieldToken.EMPTY);
        Field fieldCross = FieldToken.CROSS;

        board.setValue(new Coordinate(0,0),fieldCross);
        board.setValue(new Coordinate(1,1),fieldCross);
        board.setValue(new Coordinate(2,2),fieldCross);

        assertEquals(FieldToken.CROSS, board.areThreeSameFieldsInLine(new Coordinate(2,2)));
    }

    @Test
    public void areThreeTokensInRightDiagonal(){
        BoardField board = new BoardField(3, 3, FieldToken.EMPTY);
        Field fieldNought = FieldToken.NOUGHT;

        board.setValue(new Coordinate(0,2),fieldNought);
        board.setValue(new Coordinate(1,1),fieldNought);
        board.setValue(new Coordinate(2,0),fieldNought);

        assertEquals(FieldToken.NOUGHT, board.areThreeSameFieldsInLine(new Coordinate(2,0)));
    }

    @Test
    public void areTwoTokensInLine(){
        BoardField board = new BoardField(3, 3, FieldToken.EMPTY);
        Field fieldCross = FieldToken.CROSS;
        Field fieldNought = FieldToken.NOUGHT;

        board.setValue(new Coordinate(0,1),fieldNought);
        board.setValue(new Coordinate(0,2),fieldCross);
        board.setValue(new Coordinate(1,0),fieldNought);
        board.setValue(new Coordinate(1,1),fieldCross);
        board.setValue(new Coordinate(1,2),fieldCross);
        board.setValue(new Coordinate(2,0),fieldCross);
        board.setValue(new Coordinate(2,1),fieldNought);
        board.setValue(new Coordinate(2,2),fieldNought);

        assertEquals(FieldToken.EMPTY, board.areThreeSameFieldsInLine(new Coordinate(2,2)));
    }

}
