package Model;

import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BoardTests {

    @Test
    public void SelectedCoordinateWithinBoardFieldRange(){
        Board fieldBoard = new BoardField(3,3, FieldToken.EMPTY);

        Coordinate positiveCoordinate = new Coordinate(4,4);
        assertFalse("Coordinate bigger then board range", fieldBoard.isCoordinateValid(positiveCoordinate));

        Coordinate negativeCoordinate = new Coordinate(-1,-1);
        assertFalse("Coordinate smaller then board range", fieldBoard.isCoordinateValid(negativeCoordinate));

        Coordinate validCoordinate = new Coordinate(2,2);
        assertTrue("Coordinate within board range", fieldBoard.isCoordinateValid(validCoordinate));
    }

    @Test
    public void SelectedCoordinateWithinBoardIntegerRange(){
        Board fieldBoard = new BoardInteger(3,3);

        Coordinate positiveCoordinate = new Coordinate(4,4);
        TestCase.assertFalse("Coordinate bigger then board range", fieldBoard.isCoordinateValid(positiveCoordinate));

        Coordinate negativeCoordinate = new Coordinate(-1,-1);
        TestCase.assertFalse("Coordinate smaller board range", fieldBoard.isCoordinateValid(negativeCoordinate));

        Coordinate validCoordinate = new Coordinate(2,2);
        TestCase.assertTrue("Coordinate within board range", fieldBoard.isCoordinateValid(validCoordinate));
    }

    @Test
    public void fixedValueIsCorrectlySetToBoardField(){
        Board fieldBoard = new BoardField(3,3, FieldToken.EMPTY);
        Coordinate coordinate = new Coordinate(1,2);

        fieldBoard.setValue(coordinate, FieldToken.CROSS);
        assertEquals(FieldToken.CROSS, fieldBoard.getValue(coordinate));

        for (int i = 0; i < fieldBoard.getRowsNumber(); i++) {
            for (int j = 0; j < fieldBoard.getColsNumber(); j++) {
                if (new Coordinate(i,j).row != coordinate.row && new Coordinate(i,j).col != coordinate.col)
                    TestCase.assertEquals(FieldToken.EMPTY, fieldBoard.getValue(new Coordinate(i,j)));
            }
        }
    }

    @Test
    public void fixedValueIsCorrectlySetToBoardInteger(){
        Board integerBoard = new BoardInteger(3,3);
        Coordinate coordinate = new Coordinate(2,2);
        FieldInteger testNumber = new FieldInteger(2);
        FieldInteger zeroNumber = new FieldInteger(0);

        View.Board.printBoard(integerBoard);

        integerBoard.setValue(coordinate, testNumber);
        TestCase.assertEquals(testNumber.fieldIntegerValue, integerBoard.getValue(coordinate));

        View.Board.printBoard(integerBoard);

        for (int i = 0; i < integerBoard.getRowsNumber(); i++) {
            for (int j = 0; j < integerBoard.getColsNumber(); j++) {
                if (new Coordinate(i,j).row != coordinate.row && new Coordinate(i,j).col != coordinate.col)
                  TestCase.assertEquals(zeroNumber.fieldIntegerValue, integerBoard.getValue(new Coordinate(i,j)));
            }
        }
    }

    @Test
    public void getBoardFieldRangeIsReturnedCorrectly(){
        int rowsNumber = 5;
        int colsNumber = 6;
        Board fieldBoard = new BoardField(rowsNumber,colsNumber, FieldToken.EMPTY);

        TestCase.assertEquals(rowsNumber, fieldBoard.getRowsNumber());
        TestCase.assertEquals(colsNumber, fieldBoard.getColsNumber());
    }

    @Test
    public void getBoardIntegerRangeIsReturnedCorrectly(){
        int rowsNumber = 4;
        int colsNumber = 3;
        Board integerBoard = new BoardInteger(rowsNumber,colsNumber);

        TestCase.assertEquals(rowsNumber, integerBoard.getRowsNumber());
        TestCase.assertEquals(colsNumber, integerBoard.getColsNumber());
    }

}
