package Control;

import Model.Board;
import Model.BoardField;
import Model.Coordinate;
import Model.FieldToken;
import org.junit.Test;
import org.junit.runners.Parameterized;

import static org.junit.Assert.*;

public class PlayerHumanTests {

    @Parameterized.Parameter
    Board mockedBoard = new BoardField(3,3, FieldToken.EMPTY);

    @Test
    public void selectedCoordinateShouldNotReturnNull() {

        Coordinate coordinate = new Coordinate(2,1);
        HumanInput humanInput = new HumanMockedInput(coordinate);
        Player player = new PlayerHuman(humanInput);

        assertNotNull(player.selectMove(mockedBoard));
    }

    @Test
    public void selectedCoordinateShouldBeSameAsHumansMove(){
        Coordinate coordinate = new Coordinate(2,1);
        HumanInput humanInput = new HumanMockedInput(coordinate);
        Player player = new PlayerHuman(humanInput);

        assertEquals(player.selectMove(mockedBoard), coordinate);
    }

    @Test
    public void selectedCoordinateShouldReappearIfInvalid(){
        Coordinate coordinate = new Coordinate(2,1);
        HumanInput humanInput = new HumanMockedInput(coordinate);
        Player player = new PlayerHuman(humanInput);

        //TODO http://www.jmock.org/expectations.html

    }
}