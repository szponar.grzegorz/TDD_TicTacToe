package Control;

import Model.*;
import org.junit.Test;
import sun.invoke.empty.Empty;

import static org.junit.Assert.*;

public class GameTests {

    @Test
    public void AreInitializationObjectsCreated(){
        Game game = new Game();
        game.runGame();

        assertNotNull(game.humanInput);
        assertNotNull(game.playerCross);
        assertNotNull(game.playerNought);
        assertNotNull(game.currentBoard);
        assertNotNull(game.currentPlayer);
        assertNotNull(game.currentState);
    }

    @Test
    public void isGameFinished(){

    }

    @Test
    public void isGameWonByCross(){

    }

    @Test
    public void isGameWonByNought(){

    }

    @Test
    public void isGameDraw(){

    }

}
