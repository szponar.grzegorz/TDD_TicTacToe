package View;

import Model.Coordinate;

import org.junit.Test;

import static org.junit.Assert.*;


public class PlayerTests {

    @Test
    public void SelectedCoordinateOutsideRange(){
        Coordinate coordinate = new Coordinate(4,4);

        assertEquals("Coordinate 4, 4 cannot be selected as it is invalid.", coordinate.toString());
    }
}
