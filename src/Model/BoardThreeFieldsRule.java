package Model;


public interface BoardThreeFieldsRule {

    Field areThreeSameFieldsInLine(Coordinate lastCoordinate);

}
