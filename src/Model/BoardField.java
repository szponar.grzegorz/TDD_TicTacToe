package Model;

import java.util.Arrays;


public class BoardField implements Board, BoardThreeFieldsRule {
    Field[][] boardField;
    int rowsNumber;
    int colsNumber;

    public BoardField(int rowsNumber, int colsNumber, FieldToken fieldToFill) {
        this.rowsNumber = rowsNumber;
        this.colsNumber = colsNumber;
        boardField = new FieldToken[this.rowsNumber][this.colsNumber];
        fillBoardWithField(fieldToFill);
    }

    private void fillBoardWithField(FieldToken fieldToFill) {
        for (Field[] currentRow: boardField)
            Arrays.fill(currentRow, fieldToFill);
    }

    @Override
    public Field getValue(Coordinate currentCoordinate) {
        return boardField[currentCoordinate.row][currentCoordinate.col];
    }

    @Override
    public void setValue(Coordinate coordinate, Field fieldToken){
        boardField[coordinate.row][coordinate.col] = fieldToken;
    }

    @Override
    public boolean isCoordinateValid(Coordinate currentCoordinate) {
        return currentCoordinate.row >= 0 && currentCoordinate.row < rowsNumber &&
                currentCoordinate.col >= 0 && currentCoordinate.col < colsNumber;
    }

    @Override
    public int getRowsNumber(){
        return rowsNumber;
    }

    @Override
    public int getColsNumber(){
        return colsNumber;
    }

    @Override
    public Field areThreeSameFieldsInLine(Coordinate lastSelectedCoordinate){
        if (getValue(new Coordinate(lastSelectedCoordinate.row, 0)) == getValue(new Coordinate(lastSelectedCoordinate.row, 1))
                && getValue(new Coordinate(lastSelectedCoordinate.row,1)) == getValue(new Coordinate(lastSelectedCoordinate.row,2))
                || getValue(new Coordinate(0,lastSelectedCoordinate.col)) == getValue(new Coordinate(1,lastSelectedCoordinate.col))
                && getValue(new Coordinate(1,lastSelectedCoordinate.col)) == getValue(new Coordinate(2,lastSelectedCoordinate.col))
                || lastSelectedCoordinate.row == lastSelectedCoordinate.col            // 3-in-the-diagonal
                && getValue(new Coordinate(0,0)) == getValue(new Coordinate(1,1))
                && getValue(new Coordinate(1,1)) == getValue(new Coordinate(2,2))
                || lastSelectedCoordinate.row + lastSelectedCoordinate.col == 2  // 3-in-the-opposite-diagonal
                && getValue(new Coordinate(0,2)) == getValue(new Coordinate(1,1))
                && getValue(new Coordinate(1,1)) == getValue(new Coordinate(2,0))) {
            return getValue(new Coordinate(lastSelectedCoordinate.row, lastSelectedCoordinate.col));
        }
        return FieldToken.EMPTY;
    }
}