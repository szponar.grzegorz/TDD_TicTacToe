package Model;


public class Coordinate {
    public int row;
    public int col;

    public Coordinate(int row, int col) {
        this.row = row;
        this.col = col;
    }

//TODO exception
    @Override
    public String toString() {
        return ("Coordinate "
                + row + ", "
                + col + " cannot be selected as it is invalid.");
    }
}