package Model;


public interface Board {

    Field getValue(Coordinate currentCoordinate);

    void setValue(Coordinate currentCoordinate, Field field);

    boolean isCoordinateValid(Coordinate currentCoordinate);

    int getRowsNumber();

    int getColsNumber();
}
