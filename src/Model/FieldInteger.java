package Model;


public class FieldInteger implements Field{
    int fieldIntegerValue;

    public FieldInteger(int fieldIntegerValue) {
        this.fieldIntegerValue = fieldIntegerValue;
    }

    public String toString(){
        return String.valueOf(fieldIntegerValue);
    }
}
