package Model;


import java.util.Arrays;

public class BoardInteger implements Board {
    private Field[][] boardInteger;
    private Field zeroField = new FieldInteger(0);
    int rowsNumber;
    int colsNumber;

    public BoardInteger(int rowsNumber, int colsNumber) {
        this.rowsNumber = rowsNumber;
        this.colsNumber = colsNumber;
        boardInteger = new FieldInteger[this.rowsNumber][this.colsNumber];
        fillBoardWithField();
    }

    private void fillBoardWithField() {
        for (Field[] currentRow: boardInteger)
            Arrays.fill(currentRow, zeroField);
    }

    @Override
    public Field getValue(Coordinate currentCoordinate){
        return boardInteger[currentCoordinate.row][currentCoordinate.col];
    }

    @Override
    public void setValue(Coordinate coordinate, Field fieldInteger) {
        boardInteger[coordinate.row][coordinate.col] = fieldInteger;
    }

    @Override
    public boolean isCoordinateValid(Coordinate currentCoordinate) {
        return currentCoordinate.row >= 0 && currentCoordinate.row < rowsNumber &&
                currentCoordinate.col >= 0 && currentCoordinate.col < colsNumber;
    }

    @Override
    public int getRowsNumber(){
        return rowsNumber;
    }

    @Override
    public int getColsNumber(){
        return colsNumber;
    }
}
