package Model;


public enum FieldToken implements Field{
    EMPTY(' '), CROSS('x'), NOUGHT('o');

    private char field;

    FieldToken(char field){
        this.field = field;
    }

    public char getField(){
        return field;
    }

    public String toString(){
        return String.valueOf(getField());
    }
}
