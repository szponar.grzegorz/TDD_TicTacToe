package Control;

import Model.Coordinate;

import java.util.Scanner;

public class HumanPlayerInput implements HumanInput{

    public Coordinate getInputCoordinate(){
        Scanner in = new Scanner(System.in);
        View.Player.printSelectFieldMsg();
        return new Coordinate(in.nextInt() - 1, in.nextInt() - 1);
    }
}
