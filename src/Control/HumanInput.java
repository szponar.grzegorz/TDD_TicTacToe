package Control;

import Model.Coordinate;


public interface HumanInput {
    Coordinate getInputCoordinate();
}

