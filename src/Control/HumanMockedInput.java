package Control;

import Model.Coordinate;

public class HumanMockedInput implements HumanInput{
    Coordinate coordinate;

    public HumanMockedInput(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public Coordinate getInputCoordinate(){
        return coordinate;
    }
}
