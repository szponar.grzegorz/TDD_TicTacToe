package Control;

import Model.Board;
import Model.Coordinate;
import Model.FieldToken;

public class PlayerHuman implements Player{
    private HumanInput humanInput;

    public PlayerHuman(HumanInput humanInput) {
        this.humanInput = humanInput;
    }

    @Override
    public Coordinate selectMove(Board board){
        Coordinate selectedCoordinate;
        boolean invalidInput = true;
        do {
            selectedCoordinate = humanInput.getInputCoordinate();
            if (board.isCoordinateValid(selectedCoordinate) && isCoordinateEmpty(board, selectedCoordinate))
                invalidInput = false;
            else
                View.Player.printSelectedFieldIncorrectMsg(selectedCoordinate);
        } while (invalidInput);
        return selectedCoordinate;
    }

        private boolean isCoordinateEmpty(Board board, Coordinate selectedCoordinate) {
            return board.getValue(selectedCoordinate) == FieldToken.EMPTY;
        }
}