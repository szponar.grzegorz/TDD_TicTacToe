package Control;

import Model.*;


public class Game {
    protected int rowsNumber;
    protected int colsNumber;
    protected HumanInput humanInput;
    protected Player playerCross;
    protected Player playerNought;
    protected Board currentBoard;
    protected Player currentPlayer;
    protected State currentState;

    public void runGame() {
        gameInit();
        finishGame();
    }

        private void gameInit() {
            rowsNumber = 3;
            colsNumber = 3;
            humanInput = new HumanPlayerInput();
            playerCross = new PlayerHuman(humanInput);
            playerNought = new PlayerAiMiniMax();
            currentBoard = new BoardField(rowsNumber,colsNumber, FieldToken.EMPTY);
            currentPlayer = playerCross;
            currentState = State.PLAYING;
        }

    void updateBoard(Coordinate selectedCoordinate){

    }
    void updateState(){
        //currentState = (player == Model.Player.CROSS) ? State.CROSS_WON : State.NOUGHT_WON;
    }

    boolean isGameFinished(){
        return isGameWon() || isGameDraw();
    }

        boolean isGameWon(){
            return true;
        }

        boolean isGameDraw(){
            return true;
        }

    void finishGame(){
        View.Game.printWinner(currentPlayer);
    }
}