package Control;


import Model.Board;
import Model.Coordinate;

public interface Player {

    Coordinate selectMove(Board board);

}
