package View;

import static View.Print.*;


public class Game {

    public static void printWinner(Control.Player player) {
        printStringLn("Congratulations! " + player.toString() + " you have won!");
    }
}
