package View;

import Model.Coordinate;


public class Board {

    public static void printBoard(Model.Board currentBoard) {
        Print.printStringLn("-------");
        for (int rows = 0; rows < currentBoard.getRowsNumber(); rows++) {
            Print.printString("|");
            for (int cols = 0; cols < currentBoard.getColsNumber(); cols++) {
                Print.printString("" + getFieldFromBoardToPrint(currentBoard, rows, cols));
                Print.printString("|");
            }
            Print.printStringLn("");
        }
        Print.printStringLn("-------");
    }

    private static Model.Field getFieldFromBoardToPrint(Model.Board currentBoard, int rows, int cols) {
        return currentBoard.getValue(new Coordinate(rows, cols));
    }

}