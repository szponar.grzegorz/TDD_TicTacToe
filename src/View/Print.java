package View;


public  class Print {

    public static void printStringLn(String stringToPrint) {
        System.out.println(stringToPrint);
    }

    public static void printString(String stringToPrint) {
        System.out.print(stringToPrint);
    }

}
