package View;

import Model.Coordinate;


public class Player {

    public static void printSelectFieldMsg() {
        Print.printStringLn("Please select coordinates for the next move.");
    }

    public static void printSelectedFieldIncorrectMsg(Coordinate coordinate) {
        Print.printStringLn(coordinate.toString());
    }
}